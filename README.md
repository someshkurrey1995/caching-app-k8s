Steps to deploy app:

1. kubectl apply -f configmap.yaml
2. kubectl apply -f deployment.yaml
3. kubectl apply -f service.yaml
4. kubectl apply -f ingress.yaml

Here in config map need to provide the url for redis host, as In this deployment,
I do not installed redis internally for that we can install redis using
# docker run -d -p 6379:6379 --name=mongodb redis


Inside ingress I have added "caching-app.io" dummy webiste name you can add it on /etc/hosts to work as real application.

